# g++ -W -Wall -Wextra sayHello.cpp module1.cpp -o sayHello.out

# On peut faire un trois ligne en faisant un -c pour chaque .cpp
g++ -W -Wall -Wextra sayHello.cpp module1.cpp -c

g++ -W -Wall -Wextra sayHello.o module1.o -o sayHello.out