cmake_minimum_required( VERSION 2.6 )
project( fibo )
# -DNDEBUG pour ignorer les asserts
set( CMAKE_CXX_FLAGS "-std=c++14 -Wall -Wextra" )

find_package(PkgConfig REQUIRED)
pkg_check_modules(PKG_CPPUTEST REQUIRED cpputest)
include_directories(${PKG_CPPUTEST_INCLUDE_DIRS})

add_executable( print_fibo.out 
	src/Fibo.cpp
	src/print_fibo.cpp )


add_executable( printTest.out
	src/printTest.cpp
	src/Fibo.cpp
	src/fiboTest.cpp )

	target_link_libraries( printTest.out
	${PKG_CPPUTEST_LIBRARIES})