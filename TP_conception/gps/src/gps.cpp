/// \mainpage gps : calculateur de chemin "au plus court"
/// 
/// Permet de calculer le chemin le plus court entre deux villes, à partir d'un
/// fichier CSV contenant les routes "villeA villeB distance". Écrit, en sortie,
/// un fichier graphviz permettant de générer un graphique.
///

#include "Chemin.hpp"
#include <exception>
#include <fstream>

using namespace std;

int main(int argc, char ** argv) {

	/*
	Chemin test;
	test.importerCsv(argv[1]);
	*/

	if (argc != 5)
	{
		cerr << "usage: " << argv[0] 
			<< " <input csv> <output dot> <city1> <city2>\n";
		exit(-1);
	}

	try
	{
			Chemin chemin;
			std::ifstream ifs(argv[1]);
			chemin.importerCsv(ifs);
			std::ofstream ofs(argv[2]);
			chemin.exporterDot(ofs, argv[3], argv[4]);
	}
	catch (const string & msg)
	{
		cerr << msg << endl;
	}
	catch (const exception & e)
	{
		cerr << e.what() << endl;
	}

	return 0;
}

